known_chains = {
    "ALICE": {
        "chain_id": "6b6b5f0ce7a36d323768e534f3edb41c6d6332a541a95725b98e28d140850134",
        "core_symbol": "PPY",
        "prefix": "PPY",
    },
    "DEVNET": {
        "chain_id": "2c25aae5835fd54020329d4f150b04867e72cbd8f7f7b900a7c3da8a329a6014",
        "core_symbol": "TEST",
        "prefix": "TEST",
    },
}

