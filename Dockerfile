# FROM ubuntu-for-peerplays:latest
FROM ubuntu:20.04
MAINTAINER PeerPlays Blockchain Standards Association

RUN \
    apt-get update -y && \
      DEBIAN_FRONTEND=noninteractive apt-get install -y \
      apt-utils \
      autoconf \
      bash \
      build-essential \
      ca-certificates \
      cmake \
      dnsutils \
      doxygen \
      expect \
      git \
      graphviz \
      libboost1.67-all-dev \
      libbz2-dev \
      libcurl4-openssl-dev \
      libncurses-dev \
      libreadline-dev \
      libsnappy-dev \
      libssl-dev \
      libtool \
      libzip-dev \
      libzmq3-dev \
      locales \
      mc \
      nano \
      net-tools \
      ntp \
      openssh-server \
      pkg-config \
      perl \
      python3 \
      python3-jinja2 \
      sudo \
      wget

ENV HOME /home/peerplays
RUN useradd -rm -d /home/peerplays -s /bin/bash -g root -G sudo -u 1000 peerplays
RUN echo "peerplays  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/peerplays
RUN chmod 440 /etc/sudoers.d/peerplays

RUN service ssh start
RUN echo 'peerplays:peerplays' | chpasswd

# SSH
EXPOSE 22
# RUN service ssh status

RUN \
    apt-get update -y && \
      DEBIAN_FRONTEND=noninteractive apt-get install -y \
      redis-server
RUN sudo service redis-server start --port 6379
# RUN echo 'peerplays:peerplays' | chpasswd
EXPOSE 6379
# RUN service redis-server status

RUN \
#    apt-get update -y && \
     DEBIAN_FRONTEND=noninteractive apt-get install -y \
     libffi-dev \
     libssl-dev \
     python3-dev \
     python3-pip \
     python3-venv \
     python3-wheel
#
# WORKDIR /home/peerplays
# COPY faucet ./faucet

WORKDIR /home/peerplays/faucet
RUN mkdir /home/peerplays/.cache

# RUN chmod ugao=rwx /home/peerplays
#RUN \
#    python3 -m venv env && \
#    . env/bin/activate && \
#    pip3 install wheel # && \
# pip3 install flask-script-extras && \
ADD requirements.txt ./requirements.txt

# RUN pwd
# RUN ls

RUN pip3 install -r requirements.txt

# RUN pip3 install -r requirements.txt
# COPY *.* ./
COPY app ./app
COPY manage.py ./manage.py
COPY wsgi.ini ./
# COPY chain.py /home/peerplays/.local/lib/python3.8/site-packages/peerplaysbase/
COPY chains.py /usr/local/lib/python3.8/dist-packages/peerplaysbase/
COPY tests ./tests
COPY test.sh ./
COPY config-example.yml ./config.yml
COPY README.md ./
COPY work.py ./
# COPY start.sh ./


RUN pwd
RUN ls
# RUN mv chains.py /home/peerplays/.local/lib/python3.8/site-packages/peerplaysbase/

## ADD config.yml ./config.yml
ADD start.sh ./start.sh
#
## RUN sed -i 's/^}/    "QAENV": {\n        "chain_id": "7c1c72eb738b3ff1870350f85daca27e2d0f5dd25af27df7475fbd92815e421e",\n        "core_symbol": "TEST",\n        "prefix": "TEST",\n    },\n}/' env/lib/python3.8/site-packages/peerplaysbase/chains.py
#
## RUN sed -i 's/redis = Redis(.*/redis = Redis(host=config["redis_ip"])/'  app/views.py
#
## RUN sed -i 's/if request\.remote_addr != "127.0.0.1"/#if request.remote_addr != "127.0.0.1"/' app/views.py
## RUN sed -i 's/    return api_error("Only one account per IP")/#    return api_error("Only one account per IP")/' app/views.py
#
## RUN sed -i 's/app\.run()/app.run(host="0.0.0.0")/' manage.py
## RUN sed -i 's/app\.run(debug=True)/app.run(host="0.0.0.0", debug=True)/' manage.py
#
## Faucet
EXPOSE 5000
#
# RUN python3 manage.py install
## Faucet
# CMD ["/home/peerplays/faucet/start.sh"]
RUN chmod ugao=rwx start.sh
CMD ./start.sh
#
